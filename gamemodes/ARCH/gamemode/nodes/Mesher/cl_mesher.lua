if Mesher == nil then Mesher = {} end


function Mesher:BuildChunkMesh(Cpos)
    local res = {}
    local chunk = Arch.World[Cpos[1]][Cpos[2]][Cpos[3]]
    for x,sy in pairs(chunk) do
        for y,sz in pairs(sy) do
            for z,cell in pairs(sz) do 
                if cell[1] == 0 then goto skip_block end
                    for _,s in ipairs(Side_Check) do                       --For each side....
                        local chkcx,chkcy,chkcz = Cpos[1],Cpos[2],Cpos[3] --Curent chunk check pos
                        local chkbx,chkby,chkbz = x+s[1] ,y+s[2] ,z+s[3]  --Curent block check pos
                        if grnc[chkbx] != nil then                                 --| Check for x bordering
                            chkcx,chkbx = grnc[chkbx][1]+chkcx,grnc[chkbx][2]
                        end
                        if grnc[chkby] != nil then                                 --| Check for y bordering 
                            chkcy,chkby = grnc[chkby][1]+chkcy,grnc[chkby][2]
                        end
                        if grnc[chkbz] != nil then                                 --| Check for z bordering
                            chkcz,chkbz = grnc[chkbz][1]+chkcz,grnc[chkbz][2]
                        end

                        if !table.UberValid(Arch.World,{chkcx,chkcy,chkcz}) then goto skip end --| If chunk dosnt exist skip side check
                            --print(chkbx,chkby,chkbz)
                            if Arch.World[chkcx][chkcy][chkcz]
                                         [chkbx][chkby][chkbz][1] != 0 then goto skip end               --| 0 - nonsolid = air,render to it side
                            local light_lvl = Arch.World[chkcx][chkcy][chkcz]            --| get Lighting level of side
                                        [chkbx][chkby][chkbz][2]/10
                            for k,v in pairs(Block_Tris[_]) do
                                --print(cell,_)
                                --print(BLock_Data[cell[1]]["UV"][2])--+Block_UVs[k][2]*TSSz)
                                --PrintTable(BLock_Data[cell[1]]["UV"][_][1])
                                local CU,CV = BLock_Data[cell[1]]["UV"][_][1]+Block_UVs[k][1]*TSSz , BLock_Data[cell[1]]["UV"][_][2]+Block_UVs[k][2]*TSSz
                                table.insert(res,{
                                                color=Color(255*light_lvl,255*light_lvl,255*light_lvl),
                                                pos=Verts_Table[v]+Vector(x*Arch.Sets.BSize,y*Arch.Sets.BSize,z*Arch.Sets.BSize),
                                                u=CU,v=CV
                                            })
                            end

                        ::skip::

                    end
                ::skip_block::
            end
        end
    end
    return res
end