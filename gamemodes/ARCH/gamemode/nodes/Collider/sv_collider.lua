--|    ___________________
--|    | God Help Us All |
--|    |      __|__      |
--|    |        |        |
--|    |________|________| 
COLLIDER = {}


--  Arch.Sets.CSize
--  Arch.Sets.BSize
function COLLIDER:Calculate_CellPos(pos)
    --pos:Add( Vector(-Arch.Sets.BSize,-Arch.Sets.BSize,0))
    local cpos = Vector(pos)
    cpos:Div(Arch.Sets.BSize)
    cpos:Div(Arch.Sets.CSize)
    local chx,chy,chz = math.floor(cpos[1]),math.floor(cpos[2]),math.floor(cpos[3])

    local bpos = Vector(pos)
    bpos:Div(Arch.Sets.BSize)
    local blx,bly,blz = math.Clamp( math.floor(bpos[1])-chx*Arch.Sets.CSize+1 ,1,Arch.Sets.CSize),
                        math.Clamp( math.floor(bpos[2])-chy*Arch.Sets.CSize+1 ,1,Arch.Sets.CSize),
                        math.Clamp( math.floor(bpos[3])-chz*Arch.Sets.CSize+1 ,1,Arch.Sets.CSize)
    return {chx,chy,chz},{blx,bly,blz}
end


function COLLIDER:Trace_Vox(start,angle,leng)
    local leng = leng or 4
    local norm = angle:Forward()
    norm:Mul(Arch.Sets.BSize/4)
    local res,res1 = false
    for i=0,leng*4,1 do
        local pos = start+norm*i
        local cpos,bpos = self:Calculate_CellPos(pos)
        if table.UberValid(Arch.World,{cpos[1],cpos[2],cpos[3]}) then
            if Arch.World[cpos[1] ][cpos[2] ][cpos[3] ] [bpos[1] ][bpos[2] ][bpos[3] ][1] != 0 then
                res,res1 = cpos,bpos
                break
            end
        else
            break
        end
    end
    return res,res1
end

concommand.Add("ARCH_Trace", function(ply)
    local res,res1 = COLLIDER:Trace_Vox(ply:GetPos()+Vector(0,0,32),ply:GetAngles(),20)
    if res then
        PrintTable(res)
        PrintTable(res1)
    else
        print(res)
    end
end)

local halfblock = Arch.Sets.BSize/2
function COLLIDER:Calculate_Phys(ent,pos)
    --Super Mario 64 -like collision check,finally something good from Youtube videos in 3 AM
    local res = pos
    local steps_pos = {ent.CustVelocity,ent.CustVelocity*2,ent.CustVelocity*3}
    local res1 = false
    for k,v in pairs(steps_pos) do
        local is_skipping = false
        local hullshift = Vector(-halfblock*ent.CustHull[1],-halfblock*ent.CustHull[1],0)
        for h = ent.CustHull[2],0,-1 do
            for wy = ent.CustHull[1],0,-1 do
                for wx = ent.CustHull[1],0,-1 do
                    if is_skipping then break end
                    local step = pos+Vector(Arch.Sets.BSize*wx+v[1],Arch.Sets.BSize*wy+v[2],Arch.Sets.BSize*h+v[3])+hullshift
                    local cpos,bpos = self:Calculate_CellPos(step)
                    --print(step,pos)
                    if table.UberValid(Arch.World,{cpos[1],cpos[2],cpos[3]}) then
                        if Arch.World[cpos[1] ][cpos[2] ][cpos[3] ] [bpos[1] ][bpos[2] ][bpos[3] ][1] != 0 then
                            is_skipping = true
                        end
                    else
                        is_skipping = true
                    end
                end
            end
        end
        if !is_skipping then
            res = pos+v
        else
            res1 = true
        end
        
    end
    return res,res1
end

hook.Add("PlayerSpawn", "Add_collission_Meta", function(ply)
    ply:SetMoveType(MOVETYPE_NOCLIP)
    ply.CustPos     = Vector(10,10,480)
    ply.CustVelocity= Vector(0,0,0)
    ply.CustHull    = {1,2}
    ply.CustJump    = true
    ply.CustJumpDelay= 0
    ply:SetViewOffset(Vector(0,0,2*Arch.Sets.BSize-8))
    function ply:CalcAbsolutePosition( pos,ang )
        self:SetVelocity(self:GetVelocity()*Vector(-1,-1,-1)+Vector(0,0,8.933002))
    end 
end)

local enartia = 0.6
local fdiv = 200
local maxvel = 200
local gravity = .8
local jumpforce = 40
local jumpdelay = .5

hook.Add("SetupMove", "PlayerCalcCollission_Move", function(ply, mv, cmd )
    local pos = mv:GetOrigin()
    local ForwAxi,SideAxi = mv:GetForwardSpeed()/10000,-mv:GetSideSpeed()/10000
    local ForwardStep = Vector(mv:GetMaxSpeed()/fdiv*ForwAxi,mv:GetMaxSpeed()/fdiv*SideAxi,0)

    ForwardStep:Rotate(Angle(0,mv:GetAngles()[2],0))

    --ForwardStep:Rotate(mv:GetAngles())

    --local testion,testion1 = COLLIDER:Calculate_CellPos(ply.CustPos)
    --print(ply.CustPos,"|",testion[1],testion[2],testion[3]," ",testion1[1],testion1[2],testion1[3])
    local sgrav = gravity
    local npos,on_ground = COLLIDER:Calculate_Phys(ply,ply.CustPos)
    local velcor = ply.CustVelocity*Vector(enartia,enartia,enartia)+ForwardStep
    if on_ground then
        --sgrav = 0
        if !ply.CustJump then
            ply.CustJump = true
        end
    end
    if cmd:KeyDown(IN_JUMP) and ply.CustJump and ply.CustJumpDelay<CurTime() then
        ply.CustJump = false
        ply.CustJumpDelay = CurTime()+jumpdelay
        velcor = velcor+Vector(0,0,jumpforce)
    end
    ply.CustVelocity = Vector(math.Clamp(velcor[1],-maxvel,maxvel),
                              math.Clamp(velcor[2],-maxvel,maxvel),
                              math.Clamp(velcor[3]-sgrav,-maxvel,maxvel))
    ply.CustPos = npos
    mv:SetOrigin(npos)
    --mv:SetOrigin()
end)