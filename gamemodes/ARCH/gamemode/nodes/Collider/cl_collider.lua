local oldview = Vector(0,0,0)
local function ViewJogFix( ply, pos, angles, fov )
	local view = {} 
	
	local calc_z = ply:GetPos()+ply:GetCurrentViewOffset()
	
	view.angles = angles
	view.fov = 120
	view.drawviewer = false
	
	oldview = LerpVector( 0.1,oldview,calc_z)
	
	if oldview[3] != calc_z[3] then
		view.origin = Vector(oldview[1],oldview[2],oldview[3]--[[+1024]])
	else
		view.origin = calc_z
	end

	return view
end

hook.Add( "CalcView", "MyCalcView", ViewJogFix )