--[[
    {
        1 - block id
        2 - Light level 0-10
    }
]]
GEN = {}

util.AddNetworkString("Arch_Send_Chunk")
util.AddNetworkString("Arch_Recive_Command")
util.AddNetworkString("Arch_Chunk_Micro_Update")


function GEN:CreateChunk(Cpos)
    local res = {}
    for x=Arch.Sets.CSize,1,-1 do
        res[x] = {}
        for y=Arch.Sets.CSize,1,-1 do
            res[x][y] = {}
            for z=Arch.Sets.CSize,1,-1 do
                res[x][y][z] = {math.Round(
                                math.Clamp(
                                (perlin:noise(Cpos[1]*Arch.Sets.CSize+x,
                                             Cpos[2]*Arch.Sets.CSize+y,
                                             Cpos[3]*Arch.Sets.CSize+z,10)*20),
                                             0,3)),--10
                                 math.Round(
                                 math.Clamp(
                                 (perlin:noise(Cpos[1]*Arch.Sets.CSize+x,
                                              Cpos[2]*Arch.Sets.CSize+y,
                                              Cpos[3]*Arch.Sets.CSize+z,5)*15),
                                              0,10))
                                }
            end
        end
    end
    table.UberInsert(Arch.World,{Cpos[1],Cpos[2],Cpos[3]},res)
    return res
end

local function CoordToChunk(Cpos,Bpos)
    local chunk_count = {math.floor((Bpos[1]-1)/Arch.Sets.CSize),math.floor((Bpos[2]-1)/Arch.Sets.CSize),math.floor((Bpos[3]-1)/Arch.Sets.CSize)}
    return {Cpos[1]+chunk_count[1],
            Cpos[2]+chunk_count[2],
            Cpos[3]+chunk_count[3]}
            ,
           {Bpos[1]-chunk_count[1]*Arch.Sets.CSize,
            Bpos[2]-chunk_count[2]*Arch.Sets.CSize,
            Bpos[3]-chunk_count[3]*Arch.Sets.CSize}
end

////////// DESTROY FUNCTIONS //////////

function GEN:DestroyBlock(Cpos,Bpos)
    Arch.World[Cpos[1] ][Cpos[2] ][Cpos[3] ] 
                [Bpos[1] ][Bpos[2] ][Bpos[3] ][1] = 0
    self:MUpdateChunk(Cpos)
end

function GEN:DestroyRegion(Cpos,Bpos,radius,power) --Faster than sphere // Block destruction
    radius = radius or 2
    power  = power or 2
    for x = -radius,radius,1 do
        for y = -radius,radius,1 do
            for z = -radius,radius,1 do
                local Ccord,Bcord = CoordToChunk(Cpos,{Bpos[1]+x,Bpos[2]+y,Bpos[3]+z})
                if table.UberValid(Arch.World,Ccord) then
                    if power > BLock_Data[block]["Hard"] then
                        table.UberInsert(chunks,Ccord,true)
                        Arch.World[Ccord[1] ][Ccord[2] ][Ccord[3] ] 
                                  [Bcord[1] ][Bcord[2] ][Bcord[3] ][1] = 0
                    end
                end
            end
        end
    end
    for cx,vy in pairs(chunks) do
        for cy,vz in pairs(vy) do
            for cz,v in pairs(vz) do
                self:MUpdateChunk({cx,cy,cz})
            end
        end
    end
end

function GEN:DestroySphere(Cpos,Bpos,radius,power)
    radius = radius or 2
    power  = power or 2
    local squrR = radius*radius
    local chunks = {}
    for x = -radius,radius,1 do
        for y = -radius,radius,1 do
            for z = -radius,radius,1 do
                local Ccord,Bcord = CoordToChunk(Cpos,{Bpos[1]+x,Bpos[2]+y,Bpos[3]+z})
                if table.UberValid(Arch.World,Ccord) then
                    local block = Arch.World[Ccord[1] ][Ccord[2] ][Ccord[3] ] 
                                            [Bcord[1] ][Bcord[2] ][Bcord[3] ][1]
                    local distant = Vector(0,0,0):DistToSqr(Vector(x,y,z))
                    if distant < squrR then                       
                        if (squrR-distant)/squrR*power > BLock_Data[block]["Hard"] then
                            table.UberInsert(chunks,Ccord,true)
                            Arch.World[Ccord[1] ][Ccord[2] ][Ccord[3] ] 
                                      [Bcord[1] ][Bcord[2] ][Bcord[3] ][1] = 0
                        end
                    end
                end
            end
        end
    end
    for cx,vy in pairs(chunks) do
        for cy,vz in pairs(vy) do
            for cz,v in pairs(vz) do
                self:MUpdateChunk({cx,cy,cz})
            end
        end
    end
end


/////////// DESTRUCTS END ///////////

function GEN:MUpdateChunk(Cpos)
    local send = util.Compress(util.TableToJSON(Arch.World[Cpos[1]][Cpos[2]][Cpos[3]]))
    net.Start( "Arch_Chunk_Micro_Update" )
        print("Updating : "..#send)
        net.WriteUInt(#send,20)
        net.WriteTable( Cpos )
        net.WriteData(send,#send)
    net.Broadcast()
end

function GEN:PropagateChunk(Cpos)
    local send = util.Compress(util.TableToJSON(Arch.World[Cpos[1] ][Cpos[2] ][Cpos[3] ]))
    net.Start( "Arch_Send_Chunk" )
        print("Propagate : "..#send)
        net.WriteUInt(#send,20)
        net.WriteTable( Cpos )
        net.WriteData(send,#send)
    net.Broadcast()
end

function GEN:SendChunks() --Sending ALL server chunks to clients with delay // Created to prevent NET Overflow
    local s_delay = 0.01
    local tray = {}
    for x,sy in pairs(Arch.World) do
        for y,sz in pairs(sy) do
            for z,_ in pairs(sz) do
                table.insert(tray,{x,y,z})
            end
        end
    end
    local timersion = 0
    hook.Add("Tick", "Arch_Sender", function()
        if timersion < CurTime() then
            if #tray == 0 then
                hook.Remove("Tick", "Arch_Sender")
            goto skip end

            local Cpos = tray[#tray]
            local send = util.Compress(util.TableToJSON(Arch.World[Cpos[1] ][Cpos[2] ][Cpos[3] ]))
            net.Start( "Arch_Send_Chunk" )
                print("Propagate : "..#send)
                net.WriteUInt(#send,20)
                net.WriteTable( Cpos )
                net.WriteData(send,#send)
            net.Broadcast()
            timersion = CurTime()+s_delay

            table.remove(tray,#tray)
            
            ::skip::
        end
    end)
end


concommand.Add("ARCH_Test", function()
    local sas = 8
    for x=sas,-sas-1,-1 do
        for y=sas,-sas-1,-1 do
            for z=2,0,-1 do
                GEN:CreateChunk({x,y,z})
                --GEN:PropagateChunk({x,y,z})
            end
            if !table.UberValid(Arch.Chunks,{x,y}) then
                local chunk = ents.Create("arc_chunk")
                chunk:SetPos(Vector(Arch.Sets.CSize*Arch.Sets.BSize*x,Arch.Sets.CSize*Arch.Sets.BSize*y,0))
                chunk.CIND = {x,y}
                chunk:Spawn()
            end
        end
    end
    GEN:SendChunks()
end)

concommand.Add("ARCH_Com", function()
    net.Start( "Arch_Recive_Command" )
        net.WriteUInt(1,3)
    net.Broadcast()
end)