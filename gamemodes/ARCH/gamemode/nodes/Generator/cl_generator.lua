if Arch.Buffer == nil then Arch.Buffer = {} end


net.Receive( "Arch_Send_Chunk", function( len, ply )
    local leng = net.ReadUInt(20)
    local pos = net.ReadTable()
    local cx,cy,cz = pos[1],pos[2],pos[3]
    table.UberInsert(Arch.World,{cx,cy,cz},util.JSONToTable(util.Decompress(net.ReadData(leng))))
end) 

net.Receive( "Arch_Chunk_Micro_Update", function( len, ply )
    --print("hi")
    local leng = net.ReadUInt(20)
    local pos = net.ReadTable()
    local cx,cy,cz = pos[1],pos[2],pos[3]
    table.UberInsert(Arch.World,{cx,cy,cz},util.JSONToTable(util.Decompress(net.ReadData(leng))))

    local mesh = Mesh()
    mesh:BuildFromTriangles(Mesher:BuildChunkMesh(pos))
    table.UberInsert(Arch.Buffer,pos,mesh)
    --Arch.Chunks[cx][cy]:StartThinking_Dumbass()
end) 


local function ReloadAllChunks()
    for x,sy in pairs(Arch.World) do
        for y,sz in pairs(sy) do
            for z,_ in pairs(sz) do
                local mesh = Mesh()
                mesh:BuildFromTriangles(Mesher:BuildChunkMesh({x,y,z}))
                table.UberInsert(Arch.Buffer,{x,y,z},mesh)
                Arch.Chunks[x][y]:StartThinking_Dumbass()
            end
        end
    end
end


local Command_Switch = {
    [1]=function() ReloadAllChunks() end
}


net.Receive( "Arch_Recive_Command", function( len, ply )
    local command = net.ReadUInt(3) -- 1-Reload all chunks
    Command_Switch[command]()
end)