
function GM:PlayerNoClip( ply )
	return true
end




if Arch == nil then Arch = {} end

--[[
    Arch.Sets.CSize
    Arch.Sets.BSize
]]

Arch.Sets = {
    ["CSize"] = 10,  --DO NOT MESS WITH THIS - higher value,higher risk of bullshitery and black magic on clientside and even more undiscovered crap on serverside
    ["BSize"] = 32  --1 Block = N units // Fundamental - Changes lots of things
}

if Arch.World == nil then Arch.World = {} end
if Arch.Chunks == nil then Arch.Chunks = {} end

grnc = {
    [0] = {-1,Arch.Sets.CSize},
    [Arch.Sets.CSize+1] = {1,1}
}

corou_packs = 20


function table.UberValid(tabl,inds)
    local tab = tabl
    for k,v in pairs(inds) do
        if not tab[v] then return false end
        if k == #inds then return true end
        tab = tab[v]
    end
end


function table.UberInsert(tabl,inds,value)
    local tab = tabl
    for k,v in pairs(inds) do
        if tab[v] == nil then tab[v] = {} end
        if k == #inds then tab[v] = value end
        tab = tab[v]
    end
end

function table.UberAdd(tabl,inds,value)
    local tab = tabl
    for k,v in pairs(inds) do
        if not tab[v] then tab[v] = {} end
        tab = tab[v]
        if k == #inds then table.insert(tab,value) end
    end
end