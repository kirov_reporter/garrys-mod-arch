TSSz = 1/16
/*
      U__________ >
     V
     |
     |
     |
     v
*/

local function UVTS(u,v) -- From 1 - ....
    return {TSSz*(u-1),TSSz*(v-1)}
end


BLock_Data = {
    [0] = {
        ["Name"] = "AIR",
        ["Hard"] = 0,
    },
    [1] = {
        ["Name"] = "Moss",
        ["UV"]= {
            UVTS(2,1), -- +X  
            UVTS(2,1), -- +Y
            UVTS(1,1), -- +Z
            UVTS(2,1), -- -X
            UVTS(2,1), -- -Y
            UVTS(6,1)  -- -Z
        },
        ["Hard"] = 1,
    },
    [2] = {
        ["Name"] = "Cobble",
        ["UV"]= {
            UVTS(6,1),  
            UVTS(6,1),
            UVTS(6,1),
            UVTS(6,1),
            UVTS(6,1),
            UVTS(6,1)
        },
        ["Hard"] = 1,
    },
    [3] = {
        ["Name"] = "Stone",
        ["UV"]= {
            UVTS(3,1),  
            UVTS(3,1),
            UVTS(3,1),
            UVTS(3,1),
            UVTS(3,1),
            UVTS(3,1)
        },
        ["Hard"] = 1,
    },
    [4] = {
        ["Name"] = "Scalar",
        ["UV"]= {
            UVTS(4,1),  
            UVTS(4,1),
            UVTS(4,1),
            UVTS(4,1),
            UVTS(4,1),
            UVTS(4,1)
        },
        ["Hard"] = 1,
    },
    [5] = {
        ["Name"] = "Gravel",
        ["UV"]= {
            UVTS(5,1),  
            UVTS(5,1),
            UVTS(5,1),
            UVTS(5,1),
            UVTS(5,1),
            UVTS(5,1)
        },
        ["Hard"] = 1,
    }
}