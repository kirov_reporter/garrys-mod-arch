AddCSLuaFile("cl_init.lua")

SWEP.PrintName		= "Claws"
SWEP.Author			= "Default_OS"
SWEP.Slot			= 0
SWEP.SlotPos		= 1
SWEP.DrawAmmo		= false
SWEP.DrawCrosshair	= true
SWEP.Spawnable		= false

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Ammo			=  "none"
SWEP.Primary.Automatic		= false

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Ammo			=  "none"
SWEP.Secondary.Automatic	=  false

function SWEP:Deploy()
	self.Owner:DrawViewModel( false )
end
function SWEP:DrawWorldModel()
end

function SWEP:Initialize()
	self:SetHoldType("normal")
end

function SWEP:PrimaryAttack()
    local ply = self.Owner
    local cpos,bpos = COLLIDER:Trace_Vox(ply:GetPos()+Vector(0,0,56),ply:GetAngles(),20)
    if cpos then
        GEN:DestroyBlock(cpos,bpos)
    end
end
function SWEP:SecondaryAttack()
    local ply = self.Owner
    local cpos,bpos = COLLIDER:Trace_Vox(ply:GetPos()+Vector(0,0,56),ply:GetAngles(),20)
    if cpos then
        GEN:DestroySphere(cpos,bpos,6)
    end
end