ENT.PrintName		= "Arc_Chunk"
ENT.Author		    = "Default_OS"
ENT.Type			= "anim"
ENT.CIND            = {}

local half_chunk = Arch.Sets.CSize*Arch.Sets.BSize/2

function ENT:SetupDataTables()
 	self:NetworkVar( "Vector", 0, "ChPos" )
end

function ENT:Initialize()
    self.CIND = {self:GetChPos()[1],self:GetChPos()[2]}
    table.UberInsert(Arch.Chunks,{self.CIND[1],self.CIND[2]},self)

    self:SetRenderBounds( Vector(0,0,-16000), Vector(Arch.Sets.CSize*Arch.Sets.BSize,
                                                     Arch.Sets.CSize*Arch.Sets.BSize,16000))
    self:SetRenderAngles( Angle(360,360,360) )
end

local terrain = Material( "ARCH/TileSet" )

function ENT:off_dist()
    local s1,s2 =   LocalPlayer():GetPos()[1],LocalPlayer():GetPos()[2]
    local e1,e2 =   self.RNDR_SH[1],self.RNDR_SH[2]
    if math.Distance(s1,s2,e1,e2)<RENDER_DISTANCE then
        function self:Think() self:in_dist() end 
        self:InitVis() 
    end
end

function ENT:in_dist()
    local s1,s2 =   LocalPlayer():GetPos()[1],LocalPlayer():GetPos()[2]
    local e1,e2 =   self.RNDR_SH[1],self.RNDR_SH[2]
    if math.Distance(s1,s2,e1,e2)>RENDER_DISTANCE then
        function self:Think() self:off_dist() end 
        function self:Draw() end
    end
end


function ENT:Think()
end

function ENT:StartThinking_Dumbass()
    function self:Think()
        self.RNDR_SH = {self:GetPos()[1]+Arch.Sets.CSize*Arch.Sets.BSize/2,self:GetPos()[2]+Arch.Sets.CSize*Arch.Sets.BSize/2}
        function self:Draw() end
        function self:Think() self:off_dist() end 
    end
end


function ENT:InitVis()
    local mesh_buff = Arch.Buffer[self.CIND[1]][self.CIND[2]]
    local transion = {self:GetPos()[1]-Arch.Sets.BSize,self:GetPos()[2]-Arch.Sets.BSize}
    function self:Draw()
        render.SetMaterial(terrain)
        for z,v in pairs(mesh_buff) do
            local trans = Matrix()
                trans:Translate( Vector(transion[1],
                                        transion[2],
                                        Arch.Sets.CSize*Arch.Sets.BSize*(z)-Arch.Sets.BSize) )
            cam.PushModelMatrix( trans )
    			v:Draw()
    		cam.PopModelMatrix()
        end
    end
end